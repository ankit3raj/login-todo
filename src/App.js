import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./Components/Login";
import Dashboard from "./Components/Dashboard";
import GetTodoById from "./Components/GetTodoById";
import GetTodoByQuery from "./Components/GetTodoByQuery";
import LoginContext from "./Components/context";
import Settings from "./Components/Setting";
import { Link } from "react-router-dom";
import { useState } from "react";
import "./App.css";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userId, updateUserId] = useState("admin");
  const [passkey, updatepasskey] = useState("admin123");

  const login = () => setIsLoggedIn(true);
  const logout = () => setIsLoggedIn(false);
  const changeUserName = (name) => updateUserId(name);
  const changePassword = (pass) => updatepasskey(pass);

  return (
    <LoginContext.Provider value={{ isLoggedIn, userId, passkey }}>
      <BrowserRouter>
        <div>
          <Link to="/login">Log In</Link> <Link to="/settings">settings</Link>
        </div>
        <Routes>
          <Route path="/login" element={<Login login={login} />} />
          <Route
            path="/settings"
            element={
              <Settings
                logout={logout}
                changeUserName={changeUserName}
                changePassword={changePassword}
              />
            }
          />
          <Route path="/Dashboard" element={<Dashboard />} />
          <Route path="/todo/:todoId" element={<GetTodoById />} />
          <Route path="/todo" element={<GetTodoByQuery />} />
        </Routes>
      </BrowserRouter>
    </LoginContext.Provider>
  );
}

export default App;
