import LoginContext from "./context";
import { useState, useContext } from "react";
import { Link } from "react-router-dom";

const Settings = ({ changeUserName, changePassword }) => {
  const { isLoggedIn, userId, passkey } = useContext(LoginContext);

  console.log("isLoggedIn", isLoggedIn);
  console.log("userId", userId);
  console.log("passkey", passkey);
  const [username, setUserName] = useState("");
  const [password, setpassword] = useState("");
  const [olduser, setOlduser] = useState("");
  const [oldpass, setOldpass] = useState("");
  const [message, setMessage] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    if (olduser !== userId || oldpass !== passkey) {
      setMessage("invalid username/password");
    } else {
      changeUserName(username);
      changePassword(password);
      setMessage("Password changed");
      console.log("clicked");
    }
  };
  return (
    <div>
      <div>{isLoggedIn ? "Logged in" : "Not logged in"}</div>
      <div>
        <div className="form-container">
          <form className="form">
            <h4 className="setting-form-title">Change Username/Password</h4>
            <div className="messages">{message}</div>
            <div className="form-group ">
              <label>username</label>
              <input
                type="text"
                name="username"
                className="form-control "
                onChange={(e) => setOlduser(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label> password</label>
              <input
                type="password"
                name="password"
                className="form-control "
                onChange={(e) => setOldpass(e.target.value)}
              />
            </div>
            <div className="form-group ">
              <label className="new">New Username</label>
              <input
                type="text"
                name="newusername"
                className="form-control "
                onChange={(e) => setUserName(e.target.value)}
              />
            </div>
            <div className="form-group">
              <label className="new">New Password</label>
              <input
                type="password"
                name="newpassword"
                className="form-control "
                onChange={(e) => setpassword(e.target.value)}
              />
            </div>
            <div className="submit">
              <button
                type="submit"
                className="btn btn-primary"
                onClick={handleSubmit}
              >
                submit
              </button>
              <Link to="/login">
                <button className="btn btn-primary">Login</button>
              </Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
export default Settings;
