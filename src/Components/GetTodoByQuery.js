import React, { Component, useEffect, useState } from "react";
import { useParams, useSearchParams } from "react-router-dom";
// import { withRouter } from "react-router";
const GetTodoByQuery = (props) => {
  const [todos, setTodos] = useState([]);
  //   const { todoId } = useParams();
  //   console.log(todoId);
  const [searchParams, updateSearchParams] = useSearchParams();

  console.log(searchParams.get("id"));

  useEffect(() => {
    const todoId = searchParams.get("id");
    fetch(`https://jsonplaceholder.typicode.com/todos?id=${todoId}`)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setTodos(res);
        console.log(todos);
      });
  }, []);

  return <div>{JSON.stringify(todos)}</div>;
};

export default GetTodoByQuery;
