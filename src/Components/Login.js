import React from "react";
import { useState, useContext } from "react";
import LoginContext from "./context";
import { useNavigate } from "react-router-dom";

export default function Login({ login }) {
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [error, setError] = useState(false);
  const navigate = useNavigate();

  const { isLoggedIn, userId, passkey } = useContext(LoginContext);
  console.log(isLoggedIn);
  console.log(userId);
  console.log(passkey);

  const handleUserName = (e) => {
    setUserName(e.target.value);
    setSubmitted(false);
  };
  const handlePassword = (e) => {
    setPassword(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (userName !== userId || password !== passkey) {
      setError(true);
    } else {
      setSubmitted(true);
      login();

      setError(false);
      navigate("/dashboard");
    }
  };
  const successMessage = () => {
    return (
      <>
        <div
          className="success"
          style={{
            display: submitted ? "" : "none",
          }}
        >
          <h1>User {userName} successfully registered!!</h1>
        </div>
      </>
    );
  };
  const errorMessage = () => {
    return (
      <div
        className="error"
        style={{
          display: error ? "" : "none",
        }}
      >
        <h3>UserName/Password is incorrect</h3>
      </div>
    );
  };

  return (
    <div className="form-container">
      <form className="form">
        <h3 className="form-title">Sign In</h3>
        <div className="messages">
          {errorMessage()}
          {successMessage()}
        </div>
        <div className="form-group ">
          <label>User Name</label>
          <input
            type="text"
            className="form-control "
            placeholder="Enter username"
            value={userName}
            onChange={handleUserName}
          />
        </div>
        <div className="form-group ">
          <label>Password</label>
          <input
            type="password"
            className="form-control "
            placeholder="Enter password"
            value={password}
            onChange={handlePassword}
          />
        </div>
        <div className="submit">
          <button
            type="submit"
            className="btn btn-primary"
            onClick={handleSubmit}
          >
            Submit
          </button>
        </div>
      </form>
    </div>
  );
}
