import React, { Component, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const TodoDetails = () => {
  const [todos, setTodo] = useState([]);
  const Navigate = useNavigate();
  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/todos")
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setTodo(res.slice(0, 10));
      });
    console.log(todos);
  }, []);

  const handleClick = (id) => {
    console.log("clicked");
    Navigate(`/todo/${id}`);
  };

  return (
    <>
      <div className="container" key={todos.id}>
        <table className="table">
          <thead className="head">
            <tr className="row">
              {/* <th>User Id</th> */}
              <th>Todo id</th>
              <th>Todo</th>
              <th>status</th>
            </tr>
          </thead>
          <tbody>
            {todos.map((item) => {
              return (
                <>
                  <tr onClick={() => handleClick(item.id)}>
                    {/* <td>{item.userId}</td> */}
                    <th>{item.id}</th>
                    <th>{item.title}</th>
                    <th>{item.completed.toString()}</th>
                  </tr>
                </>
              );
            })}
          </tbody>
        </table>

        {/* <li>
              userId:{item.userId},title:{item.title},completed:
              {item.completed}
            </li> */}
      </div>
    </>
  );
};

export default TodoDetails;
