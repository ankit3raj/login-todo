import React, { Component, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
// import { withRouter } from "react-router";
const GetTodoById = (props) => {
  const [todos, setTodos] = useState([]);
  const { todoId } = useParams();
  console.log(todoId);

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/todos/${todoId}`)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        setTodos(res);
        console.log(todos);
      });
  }, []);

  return (
    <>
      <div className="container" key={todos.id}>
        <table className="table">
          <thead className="head">
            <tr className="row">
              {/* <th>User Id</th> */}
              <th>Todo id</th>
              <th>Todo</th>
              <th>status</th>
            </tr>
          </thead>
          <tbody>
            <>
              <tr>
                {/* <td>{item.userId}</td> */}
                <th>{todos.id}</th>
                <th>{todos.title}</th>
                <th>{todos.completed}</th>
              </tr>
            </>
          </tbody>
        </table>

        {/* <li>
              userId:{item.userId},title:{item.title},completed:
              {item.completed}
            </li> */}
      </div>
    </>
  );
};

export default GetTodoById;
